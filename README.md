## How to install

- pull repo
- $ composer install
- $ cp .env.example .env
- add db info to .env file
- $ php artisan migrate
- $ php artisan db:seed
- $ php artisan serve

#### Filter by owner id
/api/garages?owner_id=1

####  Filter by country id
/api/garages/country_id=1

####  Get garages near location
/api/garages?lat=60.17167429490068&lng=24.921585662024363

####  Order by price ascending order
/api/garages?order_by=price

#### Navigate results
API supports pagination, I added "meta" to JSON response. Its possible to paginate results by giving "page" attribute.
/api/garages?page=20

####  You can also mix filters as you like.
/api/garages?owner_id=2&country_id=1&order_by=price
