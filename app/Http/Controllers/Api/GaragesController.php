<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GaragesRequest;
use App\Services\GarageService;
use Illuminate\Http\Request;

class GaragesController extends Controller
{
    public function index(GaragesRequest $request)
    {
        $garageService = new GarageService();

        $garages = $garageService->getAllGarages();

        return response()->json(
            [
                'result' => true,
                'garages' => $garages['data'],
                'meta' => $garages['meta'],
            ],
            200
        );
    }
}
