<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class GaragesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'owner_id' => 'sometimes|numeric|exists:garage_owners,id',
            'country_id' => 'sometimes|numeric|exists:countries,id',
            'order_by' => 'sometimes|string|in:price',
            'lat' => 'sometimes|numeric',
            'lng' => 'sometimes|numeric',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'result' => false,
                    'message' => $validator->errors()->first(),
                ],
                422
            )
        );
    }
}
