<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GarageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'garage_id' => $this->id,
            'name' => $this->name,
            'hourly_price' => $this->hourly_price,
            'currency' => $this->currency->symbol,
            'contact_email' => $this->owner->email,
            'point' => $this->center_coordinates,
            'country' => $this->country->name,
            'owner_id' => $this->owner->id,
            'garage_owner' => $this->owner->name,
        ];
    }
}
