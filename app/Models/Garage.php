<?php

namespace App\Models;

use App\Models\Country;
use App\Models\Currency;
use App\Models\Owner;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Garage extends Model
{
    use HasFactory;

    protected $table = "garages";

    public function owner(): BelongsTo
    {
        return $this->belongsTo(Owner::class, "owner_id");
    }

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, "currency_id");
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, "country_id");
    }

    public function getCenterCoordinatesAttribute()
    {
        return $this->lat . ' ' . $this->lng;
    }
}
