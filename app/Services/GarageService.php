<?php

namespace App\Services;

use App\Http\Resources\GarageCollection;
use App\Http\Resources\GarageResource;
use App\Models\Garage;

class GarageService
{
    private $query;

    public function __construct()
    {
        $this->query = Garage::query();
    }

    public function getAllGarages()
    {
        if (request()->has('owner_id')) {
            $this->filterByOwnerId();
        }

        if (request()->has('country_id')) {
            $this->filterByCountryId();
        }

        if (request()->has('lat') && request()->has('lng')) {
            $this->filterByCoordinates();
        }

        if (request()->has('order_by')) {
            switch (request()->input('order_by')) {
                case 'price':
                    $this->orderByPrice();
                    break;
            }
        }

        $garages = $this->query->paginate(100);

        return [
            'data' => new GarageCollection($garages),
            'meta' => [
                'total' => $garages->total(),
                'current_page' => $garages->currentPage(),
                'last_page' => $garages->lastPage(),
                'per_page' => $garages->perPage(),
                'count' => $garages->count(),
            ],
        ];
    }

    private function filterByOwnerId()
    {
        $this->query->where('owner_id', request()->input('owner_id'));
    }

    private function filterByCountryId()
    {
        $this->query->where('country_id', request()->input('country_id'));
    }

    private function filterByCoordinates()
    {
        $latitude = request()->input('lat');
        $longitude = request()->input('lng');

        $this->query
            ->selectRaw("*")
            ->selectRaw(
                "6371 * acos(cos(radians(?))
                    * cos(radians(garages.lat))
                    * cos(radians(garages.lng) - radians(?))
                    + sin(radians(?))
                    * sin(radians(garages.lat))) AS distance
                ",
                [$latitude, $longitude, $latitude]
            )
            ->having('distance', '<', 20);
    }

    private function orderByPrice()
    {
        $this->query->orderBy('hourly_price', 'asc');
    }
}
