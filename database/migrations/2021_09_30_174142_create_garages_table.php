<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGaragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger("currency_id");
            $table->unsignedBigInteger("country_id");
            $table->string("name");
            $table->unsignedDecimal('hourly_price', 10, 2);
            $table->decimal('lat', 10, 8);
            $table->decimal('lng', 11, 8);
            $table->timestamps();
        });

        Schema::table('garages', function (Blueprint $table) {
            $table
                ->foreign('owner_id')
                ->references('id')
                ->on('garage_owners');

            $table
                ->foreign('currency_id')
                ->references('id')
                ->on('currencies');

            $table
                ->foreign('country_id')
                ->references('id')
                ->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garages');
    }
}
