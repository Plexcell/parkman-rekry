<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Currency;
use App\Models\Garage;
use App\Models\Owner;
use Illuminate\Database\Seeder;

class GaragesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $autopark = Owner::create([
            'name' => 'AutoPark',
            'email' => 'testemail@testautopark.fi',
        ]);

        $garage = Owner::create([
            'name' => 'Parkkitalo Oy',
            'email' => 'testemail@testgarage.fi',
        ]);

        $currency = Currency::where('code', 'EUR')->first();

        $country = Country::where('name', 'Finland')->first();

        Garage::insert([
            [
                'owner_id' => $autopark->id,
                'currency_id' => $currency->id,
                'country_id' => $country->id,
                'name' => 'Garage1',
                'hourly_price' => 2,
                'lat' => 60.168607847624095,
                'lng' => 24.932371066131623,
            ],
            [
                'owner_id' => $autopark->id,
                'currency_id' => $currency->id,
                'country_id' => $country->id,
                'name' => 'Garage2',
                'hourly_price' => 1.5,
                'lat' => 60.162562,
                'lng' => 24.939453,
            ],
            [
                'owner_id' => $autopark->id,
                'currency_id' => $currency->id,
                'country_id' => $country->id,
                'name' => 'Garage3',
                'hourly_price' => 3,
                'lat' => 60.16444996645511,
                'lng' => 24.938178168200714,
            ],
            [
                'owner_id' => $autopark->id,
                'currency_id' => $currency->id,
                'country_id' => $country->id,
                'name' => 'Garage4',
                'hourly_price' => 3,
                'lat' => 60.165219358852795,
                'lng' => 24.93537425994873,
            ],
            [
                'owner_id' => $autopark->id,
                'currency_id' => $currency->id,
                'country_id' => $country->id,
                'name' => 'Garage5',
                'hourly_price' => 3,
                'lat' => 60.17167429490068,
                'lng' => 24.921585662024363,
            ],
            [
                'owner_id' => $garage->id,
                'currency_id' => $currency->id,
                'country_id' => $country->id,
                'name' => 'Garage6',
                'hourly_price' => 2,
                'lat' => 60.16867390148751,
                'lng' => 24.930162952045407,
            ],
        ]);

        // for ($i = 0; $i < 50; ++$i) {
        //     Garage::factory()
        //         ->count(10000)
        //         ->create([
        //             'owner_id' => $autopark->id,
        //             'currency_id' => $currency->id,
        //             'country_id' => $country->id,
        //         ]);

        //     Garage::factory()
        //         ->count(10000)
        //         ->create([
        //             'owner_id' => $garage->id,
        //             'currency_id' => $currency->id,
        //             'country_id' => $country->id,
        //         ]);
        // }
    }
}
